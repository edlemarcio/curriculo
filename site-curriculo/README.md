# Website

Site para currículo construído com [Docusaurus 2](https://v2.docusaurus.io/), um moderno gerador de sites estáticos.

O currículo é publicado no [Surge](https://surge.sh). Em produção o site é publicado em  [edlemarcio.surge.sh](http://edlemarcio.surge.sh). Em em desenvolvimento  em  [edlemarcio-develop.surge.sh](http://edlemarcio-develop.surge.sh).

### Instalação

```
$ npm install
```

### Local Development

```
$ npm start
```

Inicia um servidor e abre o browser.

### Build

```
$ npm run build
```

Cria o conteúdo estático do site no diretório `build`

### Deployment


### Configure seu projeto GitLab

Surge requer autenteicação, então gere seu `token` e configure as [environment variable](https://docs.gitlab.com/ee/ci/variables/) no seu projeto GIT.


 #### SURGE_LOGIN
 
 O seu usuário do `surge`, geralmente o  seu e-mail.

 #### SURGE_TOKEN

Seu `token` gerado pelo `surge`:

```
npm install -g surge 
surge token
```