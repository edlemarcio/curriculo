---
id: doc2
title: Formação 
sidebar_label: Formação
---

## Formação  Acadêmica

* **Pós-Graduação em DESENVOLVIMENTO WEB FULL STACK 2021 - Atual**    
  * **Universidade**: PUC Minas Virtual

* **Graduação em Ciência da Computação 1999 - 2008**    
  * **Universidade**: Universidade Federal de Alagoas
  * **Título TCC**: FOX: Framework de Indexação de Objetos Java
  * **Orientador**: Evandro de Barros Costa, Klebson dos Santos Silva

## Formação Complementar

* **2020**   
  *  **SCRUM Master**  (Carga horária: 20h) APIA Treinamentos, APIA, Brasil.

* **2019**   
  * **Docker: Ferramenta essencial para Desenvolvedores.** (Carga horária: 5h) Udemy, UDEMY, Brasil.
  * **Gitlab CIT: Pipelines, Continuous Delivery e Deployment.** (Carga horária: 5h) Udemy, UDEMY, Brasil.

* **2018**   
  * **Gestão Ágil com Scrum.** (Carga horária: 7h) Udemy, UDEMY, Brasil.

* **2015**
  * **Shaping up with Angular.js.** , Code School, CODESCHOOL, Estados Unidos.
  * **Try Git.** , Code School, CODESCHOOL, Estados Unidos.
  * **JavaScript Road Trip Part 1.** , Code School, CODESCHOOL, Estados Unidos.
  * **HTML5** - Homologado pelo W3C. , Mcrisoft Virtual Academy, MVA, Estados Unidos.
  * **Try SQL.** , Code School, CODESCHOOL, Estados Unidos.
  * **Try JQuery.** , Code School, CODESCHOOL, Estados Unidos.

* **2013**
  * **SCRUM: Método Ágil de Desenvolvimento de Sistema** (Carga horária: 40h) Portal Educação, PORTAL EDUCAÇÃO, Brasil.

* **2006**
  * **Borland JBuilder 2005 Web Services** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.
  * **Borland JBuilder 2005 Web Development** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.
  * **Borland JBuilder 2005 Essential** (Carga horária: 8h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.
  * **Hibernate** (Carga horária: 25h) EFTI - Escola de Formação de Trabalhadores de Informática, EFTI, Brasil.
  * **Borland Criação de Componentes Delphi** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.
  * **Borland IDE Delphi 2005** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.

* **2000**
  * **Linguagem de Programação PERL** (Carga horária: 8h) Universidade Federal de Alagoas, UFAL, Brasil.

***
## Projetos de Pesquisa

**2015 - 2016**

Atlas do Acesso à Justiça Gestão do Conhecimento, Descrição: O projeto visa gerar conhecimento acerca do acesso à justiça no Brasil e, por meio de estudo de caso, levar à ampliação do acesso à Justiça pela sociedade brasileira, complementando e aperfeiçoando o portal Atlas de Acesso à Justiça, do Ministério da Justiça.. 

**Situação**: Concluído; 
**Natureza**: Pesquisa.
**Integrantes**: Edle Marcio dos Santos Silva - Integrante / Rafael Timoteo de sousa Junior - Coordenador.

**2014 - 2015**

Acordo de Cooperação Técnica FUB/CDT e MP/SEGEP/AUDIR - Projeto SIGA - Sistema de Inteligência e Gestão da Auditoria., Descrição: O projeto de pesquisa e desenvolvimento tem base nos domínios das tecnologias de sistemas distribuídos e redes, arquitetura da informação e exploração multidimensional, modelagem de processos e sistemas e gerenciamento de projetos e governança de TI. 
Integrados em um processo de gerenciamento e embasados em uma modelagem rigorosa da informação necessária, os resultados dessa pesquisa serão aplicados para apoiar a SEGEP, com a integração de novos processos e metodologias através de um protótipo de um sistema de inteligência gerencial da auditoria interna do MP. A cooperação FUB-SEGEP se dará de modo a permitir à SEGEP a absorção de metodologias baseadas em melhores práticas internacionais nos domínios de serviços de TI, gerenciamento de projetos e desenvolvimento de software.. , 

**Situação**: Concluído; 
**Natureza**: Pesquisa. 
**Integrantes**: Edle Marcio dos Santos Silva - Integrante / Rafael Timoteo de sousa Junior - Coordenador.