---
id: dica_docker_uteis
title: Docker - Comandos úteis
sidebar_label: Docker - Comandos úteis
---
## Containers úteis 


### Comandos para rodar o Container

#### Rodar o Portainer

``docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce``

Volume para o windows (wsl) = "//var/run/docker.sock:/var/run/docker.sock"

#### Rodar um container especificando uma rede

``docker run -d --network nome-da-rede --name nome-container nome-imagem:tag``

#### Rodar um container especificando um volume

``docker run -d --network nome-da-rede -v /pasta/maquina/local:/pasta/no/container --name nome-container nome-imagem:tag`` 

``docker run -d --network nome-da-rede -v nome-do-volume:/pasta/no/container --name nome-container nome-imagem:tag`` 


### Comandos para rodar o MySQL

``docker run --name meu_mysql --restart=always -v meu_mysql_data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=minhasenha -d -p 3306:3306 mariadb:10.4.14``

``docker exec -it meu_mysql bash``
``CREATE USER 'nome_usuario'@'localhost' IDENTIFIED BY 'senhaUsuario';``
``GRANT ALL PRIVILEGES ON * . * TO 'cura'@'localhost';``

### Comandos para rodar o MyAdmin

#### Cria um container chamado meu_myadmim

``docker run --name meu_myadmin  --restart=always -d -e MYSQL_ROOT_PASSWORD=minhasenha  -p 8088:80 phpmyadmin/phpmyadmin:5.0``

#### Cria um container chamado meu_myadmim e link com um container do MySQL

``docker run --name meu_myadmin  --restart=always -d -e MYSQL_ROOT_PASSWORD=minhasenha --link cura_mysql:db -p 8088:80 phpmyadmin/phpmyadmin:5.0``


### Comandos para atualizar o Container com uma nova imagem

#### Para o container

``docker container stop portainer &&``

#### Remove o container

``docker container rm portainer &&``

#### Cria um novo container ver _Comandos para rodar o Container_


### Comandos para salvar uma image para arquivo

#### Salvar uma imagem para o arquivo

``docker image save nome-imagem/tag > nome-do-arquivo-para-salvar-image.tar``

#### Salvar uma imagem para o arquivo e compacata utilizando o gzip

``docker image save sia-frontend/dsv:1.1.0 | gzip > nome-do-arquivo-para-salvar-image.tar.gz``

### Comandos para carregar uma image de um arquivo

#### Carrega uma imagem de um arquivo

``docker load < nome-do-arquivo-para-salvar-image.tar``


