---
id: dica_docker_basicos
title: Docker - Comandos básicos
sidebar_label: Docker - Comandos básicos
---

export const Highlight = ({children, color}) => ( <span style={{
      backgroundColor: color,
      borderRadius: '2px',
      color: '#fff',
      padding: '0.2rem',
    }}> {children} </span> );

## Objetivo

Mini guia rápido para consultar os principais comandos do docker, facilitando o dia a dia do desenvolvedor.

## Comandos básicos

###  Baixando uma imagem

-  ``docker pull ubuntu``
-  ``docker pull ubuntu:14.04``


###  Rodando container de imagens

Para rodar uma imagem digite o comando: **docker image run <Highlight color="#25c2a0">_nome_image:versao_</Highlight>**

  -  ``docker container run ubuntu``
  -  ``docker container run ubuntu:14.04``
  -  ``docker container run --name meu-container ubuntu:14.04``
  -  ``docker container run -d --name meu-container ubuntu:14.04``
  -  ``docker container run --name mynginx1 -p 88:80 -d nginx`` 
  
  *Observação: * _--publish_  ou _-p_  permite publicar a porta do container no host (máquina atual): 

  -  ``docker container run --name mynginx1 -p host:container -d nginx``
  
### Rodar image e manter o container docker rodando com o prompt

-  ``docker container run ubuntu bash``

### Listando containers

- Processo em execução
  -  ``docker container ps`` 
- Todos os processos
  -  ``docker container ps -a`` 

### Rodando um containers existente

-  ``docker container start NOME_CONTAINER``

### Parando um containers

-  ``docker container stop NOME_CONTAINER``

### Reinicializando um containers

-  ``docker container restart NOME_CONTAINER``

### Removendo um containers

-  ``docker container rm NOME_CONTAINER``

> **NOME_CONTAINER** ou **ID** do  container  informado no comando  **Listando containers**

### Acessando um containers rodando

-  ``docker container attach NOME_CONTAINER``

### Rodando um comando num containers rodando

-  ``docker container exec [OPÇÕES] NOME_CONTAINER`` COMMAND [ARGS...]
-  ``docker container exec -it ubuntu_bash bash``

##  **Docker**

### O que é Docker?

O [Docker](https://docs.docker.com/) fornece uma maneira de executar aplicativos isolados com segurança em um contêiner, empacotado com todas as suas dependências e bibliotecas.

Docker é uma ferramenta que cria “micro maquinas virtuais” com a finalidade de não “sujar” o ambiente do desenvolvedor.

### O que é uma imagem?

**Imagem** é como se fosse uma ISO de um sistema operacional. Serve para inicializar uma máquina virtual e se tornar um container. Com uma mesma imagem podemos rodar diversos containers.

### O que é um container?

**Container** é uma máquina virtual pronta para uso inciada a partir de uma imagem. 


<!-- <Highlight color="#25c2a0">Docusaurus green</Highlight> and <Highlight color="#1877F2">Facebook blue</Highlight> are my favorite colors.
 -->


