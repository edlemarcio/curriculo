---
id: doc1
title: Apresentação
sidebar_label: Apresentação
---

# Édle Márcio dos Santos Silva - 43 anos
***

Sou bacharel em Ciência da Computação pela **Universidade Federal de Alagoas - UFAL** certificado **SCRUM MASTER PROFESSIONAL CERTIFICATE (SMPC®)** pela **CertiProf**. Atua profissionalmente em desenvolvimento de sistemas desde 2002.

Atualmente sou _Arquiteto de Software_ no **Tribunal Superior Eleitoral - TSE** pela **[G4F](https://www.g4f.com.br/)**. Atuando como Arquiteto de Sistemas utilizando os padrões J2EE (Java, JSP, Servlets, EJBs) e Spring Boot com banco de dados Oracle; Como Arquiteto corporativo, participando de propostas de modelo arquiteturais, apoiando a adoção de tecnologias e da Integração, Entrega e Implantação Contínua. Tenho experiência em desenvolvimento **front-end**, com AngularJS, Angular.io, Primefaces ou simplesmente Javascript, HTML e CSS. Tenho conhecimento em tecnologias como Node.js e bancos de dados orientados a documentos (NoSQL). 

Gosto de trabalhar:

 * **Com Agilidade** - gestão e desenvolvimento ágil, utilizando _Scrum_ e _Integração contínua (CI) e Entrega contínua (CD)_ com ferramentas open-source como Maven, Git, Gitlab e Jenkins;
 * **Em equipe**: juntos vamos mais longe! 
 * **Com compromisso** - Ser leal a equipe e aos objetivos do projeto e da instituição, manter o foco para atingir os resultados que agregam mais valor;  
