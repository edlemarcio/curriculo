---
id: doc3
title: Histórico profissional
sidebar_label: Histórico profissional
---

## 02-2022 - Atual

_**G4F (TSE)**_
Vínculo: Arquiteto de Software, Regime: Dedicação exclusiva.

**Outras informações:**
Participação em propostas de modelo arquiteturais, apoiando a adoção de tecnologias e da Integração, Entrega e Implantação Contínua.  

## 07-2021 - 02-2021

_**Engesoftware Tecnologia**_
Vínculo: Arquiteto de Software, Regime: Dedicação exclusiva.

**Outras informações:**
Participação no desenvolvimento e modelagem da arquitetura Spring Boot e Angular do Sistema de Informações sobre Agrotóxicos - SIA (MAPA, IBAMA e ANVISA). 


## 01-2021 - 07-2021

_**CTIS Tecnologia - Empresa do Grupo Sonda (TSE)**_
Vínculo: Engenheiro de Software, Regime: Dedicação exclusiva.

**Outras informações:**
Participação no desenvolvimento e modelagem da arquitetura Java do Sistema de Integração da Prestação de Contas Partidárias - SPCA com o PJE via mensageria. 

## 03-2020 - 01-2021

_**Engesoftware Tecnologia (TSE)**_
Vínculo: Engenheiro de Software, Regime: Dedicação exclusiva.

**Outras informações:**
Participação no desenvolvimento e modelagem da arquitetura Java do Sistema de Integração da Prestação de Contas Eleitoral- SPCE com o PJE via mensageria. 
## 07-2009 - 03-2020

_**CTIS Tecnologia  (TSE)**_
Vínculo: Engenheiro de Software, Regime: Dedicação exclusiva.

**Outras informações:**
Participação no desenvolvimento e modelagem da arquitetura Java do Sistema de Prestação de Contas Eleitoral - SPCE. Participação na integração de Sistemas de Extratos Bancários. Participação na definição e implantação da Arquitetura Java para desenvolvimentos de sistemas corporativos do setor.

## 2015 - 2016

_**Universidade de Brasília, UnB**_
Vínculo: Bolsista, Enquadramento Funcional: Analista de Sistema, Carga horária: 20

**Outras informações:**
Participei do termo de cooperação celebrado entre MJ/SRJ e FUB por intermédio do CDT- Atlas do Acesso à Justiça Gestão do Conhecimento.

## 2014 - 2015

_**Universidade de Brasília, UnB**_
Vínculo: Bolsista, Enquadramento Funcional: Desenvolvedor Java, Carga horária: 20

**Outras informações:**
Participei do acordo de cooperação FUB/CDT e MP/SEGEP/AUDIR - Projeto SIGA - Sistema de Inteligência e Gestão da Auditoria.

## 2006 - 2009

_**Poliedro Informática Consultoria e Serviços  (FNDE)**_
Vínculo: Enquadramento Funcional: Analista de Sistema, Regime: Dedicação exclusiva.

**Outras informações:**
* _**Arquiteto Java/Oracle**_: no SIGAP (Sistema Integrado de Gestão e Acompanhamento de Prestação de Contas) Analista/Desenvolvedor Java - WebPrevia (Sistema de simulação/divulgação de processos financeiros - módulo web do SCF); 
* _**Analista Desenvolvedor Java**_: no WebRH (Sistema de Recursos Humanos On-line); 
* _**Analista Desenvolvedor Delphi/Oracle**_: no  SCF/RFB (Sistema de exportação de processo do FNDE para a Receita Federal do Brasil) e no SCF (Sistema Financeiro do Fundo Nacional de Desenvolvimento da Educação);
* _**Analista Desenvolvedor Delphi/MSSQL**_: no CBG (Censo Bibliográfico de Graduação do MEC/2006 - Controle de estoque e entrega de livros).

## 2002 - 2005

_**Universidade Federal de Alagoas**_
Vínculo: Bolsista, Enquadramento Funcional: Desenvolvedor PHP, Carga horária: 4

**Outras informações:**
* **Desenvolvedor PHP/MSSQL** - Portal e Central de Sistemas de Gestão Acadêmica via Web da Universidade Federal de Alagoas desenvolvido em PHP, 100% orientado a objetos, base MSSQL Server e Apache HTTP Server (http://www.ufal.br).

## 2001 - 2005

_**Microcamp Tecnologia**_
Vínculo: Instrutor de Informática, Carga horária: 4

**Outras informações:**
* **Instrutor de Web Design**:  Ministrava aulas dos seguintes módulos: Windows Me/2000/XP, Internet, HTML, CSS, DHTML, Abode Photoshop, Corel Draw, Javascript, Macromedia Flash MX, Macromedia Dreamweaver MX entre outros. 
* **Instrutor de Linguagens de Programação**: Ministrava aulas de Algoritmo, Pascal, Borland Delphi (Object Pascal), PHP, Banco de dados MySQL entre outros.