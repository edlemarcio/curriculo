module.exports = {
  title: 'Édle Márcio dos Santos Silva',
  tagline: 'Sou bacharel em Ciência da Computação pela Universidade Federal de Alagoas - UFAL e certificado SCRUM MASTER PROFESSIONAL CERTIFICATE (SMPC®) pela CertiProf.',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'edlemarcio', // Usually your GitHub org/user name.
  projectName: 'https://gitlab.com/edlemarcio/curriculo', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Édle Márcio',
      logo: {
        alt: 'Édle Márcio (Foto Google)',
        src: 'https://lh3.googleusercontent.com/-f1l0cW8C4lI/AAAAAAAAAAI/AAAAAAAAAAA/AAKWJJOlu7YQSCjU3_RCYs3kzS3fFx1qGA.CMID/s83-c/photo.jpg',
      },
      items: [
        {
          to: 'docs/doc1',
          activeBasePath: 'docs',
          label: 'Currículo',
          position: 'left',
        },
        {
          to: 'docs/dicas/dica_docker_basicos',
          activeBasePath: 'docs',
          label: 'Dicas',
          position: 'left',
        },
        // {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/edlemarcio/curriculo',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        // {
        //   title: 'Currículo',
        //   items: [
        //     {
        //       label: 'Style Guide',
        //       to: 'docs/doc1',
        //     },
        //     {
        //       label: 'Second Doc',
        //       to: 'docs/doc2',
        //     },
        //   ],
        // },
        {
          title: 'Tecnologias Usadas',
          items: [
            {
              label: 'Docusaurus',
              href: 'https://docusaurus.io/',
            },
            {
              label: 'Surge',
              href: 'https://surge.sh/',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'LinkedIn',
              href: 'https://www.linkedin.com/in/edlemarcio/',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/edlemarcio/curriculo',
            },
            {
              label: 'Currículo Lattes',
              href: 'http://lattes.cnpq.br/3384353968179685',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Edle Márcio, edlemarcio@gmail.com,  Built with Docusaurus.<br> Versão 1.0.3.01`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/edlemarcio/curriculo/edit/master/site-curriculo/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
