import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <>Trabalho em equipe</>,
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        "Eu sou parte de uma equipe. Então, quando venço, 
        não sou eu apenas quem vence. De certa forma,
         termino o trabalho de um grupo enorme de pessoas."<br/>
         <br/><strong>Ayrton Senna</strong>
      </>
    ),
  },
  {
    title: <>Melhoria contínua</>,
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
      A cada sucesso, erro ou fracasso, o intuito é sempre aprender. A equipe deve ser estimulada e habilitada ao aprendizado constante, buscando sempre o crescimento individual e do grupo.
      </>
    ),
  },
  {
    title: <>Produtividade</>,
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
      “Melhorando a qualidade, automaticamente você estará melhorando a produtividade.” <br/>
      <br/><strong>David Allen</strong> <em>consultor e criador da famosa metodologia GTD</em>
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Currículo`}
      description="Engenheiro de Software <head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/doc1')}>
              Currículo
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
