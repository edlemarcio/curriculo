# Édle Márcio dos Santos Silva
***

Sou bacharel em Ciência da Computação pela **Universidade Federal de Alagoas - UFAL** certificado **SCRUM MASTER PROFESSIONAL CERTIFICATE (SMPC®)** pela **CertiProf**. Atua profissionalmente em desenvolvimento de sistemas desde 2002.

Atualmente sou _Engenheiro de Software_ no **Tribunal Superior Eleitoral - TSE** pela **Engesoftware – Tecnologia S.A**. Atuando como Desenvolvedor e Arquiteto de Sistemas utilizando os padrões J2EE (Java, JSP, Servlets, EJBs) e Spring Boot com banco de dados Oracle. Tenho experiência em desenvolvimento **front-end**, com AngularJS, Angular.io, Primefaces ou simplemente Javascript, HTML e CSS. Também tenho conhecimento em tecnologias como Node.js e bancos de dados orientados a documentos (NoSQL). 

Gosto de trabalhar:

 * **Com Agilidade** - gestão e desenvolvimento ágil, utilizando _Scrum_ e _Integração contínua (CI) e Entrega contínua (CD)_ com ferramentas open-source como Maven, Git, Gitlab e Jenkins;
 * **Em equipe**: juntos vamos mais longe! 
 * **Com compromisso** - Ser leal a equipe e aos objetivos do projeto e da instituição, manter o foco para antingir os resultados que agregam mais valor;  


# Currículo

***
## Formação Acadêmica

* **Graduação em Ciência da Computação 1999 - 2008**
    _Universidade Federal de Alagoas_
    _Título TCC_: FOX: Framework de Indexação de Objetos Java
    _Orientador_: Evandro de Barros Costa, Klebson dos Santos Silva

***
## Formação Complementar

* **2020**
  * **SCRUM Master**. (Carga horária: 20h) APIA Treinamentos, APIA, Brasil.

* **2019**
  * **Docker: Ferramenta essencial para Desenvolvedores.** (Carga horária: 5h) Udemy, UDEMY, Brasil.

  * **Gitlab CIT: Pipelines, Continuous Delivery e Deployment.** (Carga horária: 5h) Udemy, UDEMY, Brasil.

* **2018**
  * **Gestão Ágil com Scrum.** (Carga horária: 7h) Udemy, UDEMY, Brasil.

* **2015**
    * **Shaping up with Angular.js.** , Code School, CODESCHOOL, Estados Unidos.
    * **Try Git.** , Code School, CODESCHOOL, Estados Unidos.
    * **JavaScript Road Trip Part 1.** , Code School, CODESCHOOL, Estados Unidos.
    * **HTML5** - Homologado pelo W3C. , Mcrisoft Virtual Academy, MVA, Estados Unidos.
    * **Try SQL.** , Code School, CODESCHOOL, Estados Unidos.
    * **Try JQuery.** , Code School, CODESCHOOL, Estados Unidos.

* **2013**
    * **SCRUM: Método Ágil de Desenvolvimento de Sistema** (Carga horária: 40h) Portal Educação, PORTAL EDUCAÇÃO, Brasil.

* **2006**
    * **Borland JBuilder 2005 Web Services** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.

    * **Borland JBuilder 2005 Web Development** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.

    * **Borland JBuilder 2005 Essential** (Carga horária: 8h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.

    * **Hibernate** (Carga horária: 25h) EFTI - Escola de Formação de Trabalhadores de Informática, EFTI, Brasil.

    * **Borland Criação de Componentes Delphi** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.

    * **Borland IDE Delphi 2005** (Carga horária: 20h) BLUESTAR INFORMATICA LTDA, BI_FORN, Brasil.

* **2000**
    * **Linguagem de Programação PERL** (Carga horária: 8h) Universidade Federal de Alagoas, UFAL, Brasil.

***
## Projetos de Pesquisa

**2015 - 2016**

Atlas do Acesso à Justiça Gestão do Conhecimento, Descrição: O projeto visa gerar conhecimento acerca do acesso à justiça no Brasil e, por meio de estudo de caso, levar à ampliação do acesso à Justiça pela sociedade brasileira, complementando e aperfeiçoando o portal Atlas de Acesso à Justiça, do Ministério da Justiça.. 

**Situação**: Concluído; 
**Natureza**: Pesquisa.
**Integrantes**: Edle Marcio dos Santos Silva - Integrante / Rafael Timoteo de sousa Junior - Coordenador.

**2014 - 2015**

Acordo de Cooperação Técnica FUB/CDT e MP/SEGEP/AUDIR - Projeto SIGA - Sistema de Inteligência e Gestão da Auditoria., Descrição: O projeto de pesquisa e desenvolvimento tem base nos domínios das tecnologias de sistemas distribuídos e redes, arquitetura da informação e exploração multidimensional, modelagem de processos e sistemas e gerenciamento de projetos e governança de TI. 
Integrados em um processo de gerenciamento e embasados em uma modelagem rigorosa da informação necessária, os resultados dessa pesquisa serão aplicados para apoiar a SEGEP, com a integração de novos processos e metodologias através de um protótipo de um sistema de inteligência gerencial da auditoria interna do MP. A cooperação FUB-SEGEP se dará de modo a permitir à SEGEP a absorção de metodologias baseadas em melhores práticas internacionais nos domínios de serviços de TI, gerenciamento de projetos e desenvolvimento de software.. , 

**Situação**: Concluído; 
**Natureza**: Pesquisa. 
**Integrantes**: Edle Marcio dos Santos Silva - Integrante / Rafael Timoteo de sousa Junior - Coordenador.

***
## Histórico profissional

**03/2020 - Atual**

_**Engesoftware Tecnologia**_
Vínculo: Engenheiro de Software, Regime: Dedicação exclusiva.

**07/2009 - 03/2020**

_**CTIS Tecnologia**_
Vínculo: Engenheiro de Software, Regime: Dedicação exclusiva.

**Outras informações:**
Participação no desenvolvimento e modelagem da arquitetura Java do Sistema de Prestação de Contas Eleitoral - SPCE. Participação na integração de Sistemas de Extratos Bancários. Participação na definição e implantação da Arquitetura Java para desenvolvimentos de sistemas corporativos do setor.

**2015 - 2016**

_**Universidade de Brasília, UnB**_
Vínculo: Bolsista, Enquadramento Funcional: Analista de Sistema, Carga horária: 20

**Outras informações:**
Participo do termo de cooperação celebrado entre MJ/SRJ e FUB por intermédio do CDT- Atlas do Acesso à Justiça Gestão do Conhecimento.

**2014 - 2015**

_**Universidade de Brasília, UnB**_
Vínculo: Bolsista, Enquadramento Funcional: Desenvolvedor Java, Carga horária: 20

**Outras informações:**
Participei do acordo de cooperação FUB/CDT e MP/SEGEP/AUDIR - Projeto SIGA - Sistema de Inteligência e Gestão da Auditoria.


**2006 - 2009**

_**Poliedro Informática Consultoria e Serviços**_
Vínculo: Enquadramento Funcional: Analista de Sistema, Regime: Dedicação exclusiva.

**Outras informações:**
* _**Arquiteto Java/Oracle**_: no SIGAP (Sistema Integrado de Gestão e Acompanhamento de Prestação de Contas) Analista/Desenvolvedor Java - WebPrevia (Sistema de simulação/divulgação de processos financeiros - módulo web do SCF); 
* _**Analista Desenvolvedor Java**_: no WebRH (Sistema de Recursos Humanos On-line); 
* _**Analista Desenvolvedor Delphi/Oracle**_: no  SCF/RFB (Sistema de exportação de processo do FNDE para a Receita Federal do Brasil) e no SCF (Sistema Financeiro do Fundo Nacional de Desenvolvimento da Educação);
* _**Analista Desenvolvedor Delphi/MSSQL**_: no CBG (Censo Bibliográfico de Graduação do MEC/2006 - Controle de estoque e entrega de livros).

**2002 - 2005**

_**Universidade Federal de Alagoas**_
Vínculo: Bolsista, Enquadramento Funcional: Desenvolvedor PHP, Carga horária: 4

**Outras informações:**
* **Desenvolvedor PHP/MSSQL** - Portal e Central de Sistemas de Gestão Acadêmica via Web da Universidade Federal de Alagoas desenvolvido em PHP, 100% orientado a objetos, base MSSQL Server e Apache HTTP Server (http://www.ufal.br).

**2001 - 2005**

_**Microcamp Tecnologia**_
Vínculo: Instrutor de Informática, Carga horária: 4

**Outras informações:**
* **Instrutor de Web Design**:  Ministrava aulas dos seguintes módulos: Windows Me/2000/XP, Internet, HTML, CSS, DHTML, Abode Photoshop, Corel Draw, Javascript, Macromedia Flash MX, Macromedia Dreamweaver MX entre outros. 
* **Instrutor de Linguagens de Programação**: Ministrava aulas de Algoritmo, Pascal, Borland Delphi (Object Pascal), PHP, Banco de dados MySQL entre outros.

<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="color:#DA7059; text-align: center;">Versão 1.0.1.00</p>
